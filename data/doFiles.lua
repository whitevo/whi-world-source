-- io.popen has do be done differently for linux and windows

local function dofile_folder(path, folderNames)
    if testServer() then
        if folderNames then
            for _, folderName in ipairs(folderNames) do
                local path = path.."/"..folderName
                dofile_folder(path)
            end
        end

        for fileName in io.popen([[dir "./data/]]..path..[[" /b /aa]]):lines() do dofile("data/"..path.."/"..fileName) end
    else
        if folderNames then
            for _, folderName in ipairs(folderNames) do
                local path = path.."/"..folderName
                for fileName in io.popen([[ls ./data/]]..path):lines() do dofile("data/"..path.."/"..fileName) end
            end

            for fileName in io.popen([[ls ./data/]]..path):lines() do
                if not isInArray(folderNames, fileName) then dofile("data/"..path.."/"..fileName) end
            end
        else
            for fileName in io.popen([[ls ./data/]]..path):lines() do dofile("data/"..path.."/"..fileName) end
        end
    end
end

dofile_folder("global_functions")
--dofile('data/storageValues.lua')
--dofile('data/itemID table.lua')
--dofile('data/special ID table.lua')

--dofile('data/items/items conf.lua')
--dofile('data/central system.lua')
--dofile_folder("game_systems")
--dofile('data/monster/monsters conf.lua')

--dofile('data/npc/npcSystem conf.lua')   
--dofile('data/npc/npcChat conf.lua')
--dofile('data/npc/shopSystem conf.lua')
--dofile('data/npc/npcChat.lua')
--dofile('data/npc/shopSystem.lua')
--dofile('data/npc/npcRepSystem.lua')
--dofile('data/npc/npcSystem.lua')
--dofile('data/spells/areas.lua')
--dofile('data/spells/playerSpells.lua')
--featureFolders = {"other", "special_items", "jazMazFeatures", "containers", "professions", "upgrades", "environment", "machines"}
--dofile_folder("features", featureFolders)
--dofile_folder("npc/npcs")
--dofile_folder("areas", {"regions"})
--dofile_folder("missions")
--dofile_folder("Quests")
--dofile_folder("games")
--local spellFolders = {"hunter", "druid", "knight", "mage", "talents", "jazmaz_monsterSpells"}
--for _, folderName in ipairs(spellFolders) do dofile_folder("spells/"..folderName) end
--dofile('data/spells/spellBuffs.lua')
--dofile('data/spells/monster spells conf.lua')
--dofile('data/spellCreatingSystem.lua')
--dofile_folder("monster/monsters", {"bosses"})
--dofile('data/creaturescripts/scripts/HISTORY.lua')