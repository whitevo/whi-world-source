--[[ itemTable config guide
    [INT] = {                       item AID/ID
        allowFarUse = BOOL or false
        blockWalls = BOOL or true
        checkFloor = BOOL or true
        
        onlyPlayer = BOOL or true   if true then if the action activator is not a player then RETURNS nil
        
        timers = {
            time = INT              Item will trigger once every INT seconds globally.
            guidTime = INT          Item will trigger once every INT seconds for the player guid.
            text = INT              Item text what comes up when can't use yet
            showTime = BOOL         true - shows time remaining before can be clicked again.
                                    NB! must be used along with text, becase all it does it adds timeText as last part of string.
        },
        
        mana = INT                  gives INT amount of MP to player
        health = INT                gives INT amount of HP to player
        exp = INT                   gives INT amount of experience
        
        text = { or textF           textF = message when action system didn't activate
            useOnFail = false       if true then same text will be used when conditions have failed  (in others words its used as textF)
            type = ENUM or GREEN    ORANGE, GREEN, RED, WHITE, BLUE
            msg = {STR}             if table used, then splits them up in different sentences.
            text = {                text what appers on spefic location
                msg = STR       
                position = POS or itemPos    postion where text is put
            }
            svText = {
                [{INT, INT}] = {    first INT is storage value | second INT what storage value must be
                    msg = STR
                    type = ENUM or GREEN
                }
            }
        },
        
        rewardItems = {{            player gets items in this table
            itemID = INT or {}      if itemID is table then choose 1 id randomly from table.
            count = INT or 1        amount of items
            itemAID = INT or {}     if itemAID is table then choose 1 aid randomly from table.
            type = INT              if used then sets item type
            itemText = STR          if item should have TEXT -- itemTextMOD(creature, item, text)
                                    "randomStats" > items_randomiseStats(item)
                                    "playerName" > creature:getName()
                                    "accountID" > creature:getAccountId()
            itemName = STR or item:getName()
        }},
        
        hasItems or hasItemsF = {{  hasItemsF is true when player doesn't have the items
            itemID = INT,
            count = INT or 1,
            itemAID = INT,
            fluidType = INT,
        }},
        takeItems = {{
            itemID = INT,
            count = INT or 1,
            itemAID = INT,
            fluidType = INT,
        }},
        
        allSV = {[SV] = V}          If all of these storage values match, then action triggers
        setSV  = {[SV] = V}         player:setStorageValue(K, V)
        bigSV  = {[SV] = V}         storage values have to same or bigger
        addSV  = {[SV] = V}
        anySV  = {[SV] = {V}}       if any of these storage values match, then action triggers
                                    put values in table if there are several "ok" values in 1 SV
        
        ME = {                      Magic effect maker
            pos = POS or itemPos    POS = location for the magic effect to appear | STR = position is generated on action >> itemPos = item:getPosition()
            effects = INT or {}     Effects what are done in secuence 
            interval = INT or 0     the time between effects in milliseconds
        },
        
        transform = {
            itemID = INT or item:getId()            change into INT item. | if INT == 0 then removes item
            itemAID = INT or item:getActionId()     if INT < 100 then removes itemAID
            returnDelay = INT                       in milliseconds when item turns back
        },
        
        createMonster = {{
            name = STR              name of the monster
            pos = POS or itemPos    position for monster
            count = INT or 1        how many monsters
        }}
        
        createItems = {{
            itemID = INT or item:getId()
            itemAID = INT           if not itemID then DEFAULT item:getActionId()
            type = INT
            count = INT or 1
            pos = POS or itemPos
            delay = MSec            how many milliseconds later item is created
        }}
        
        removeItems = {{
            itemID = INT or item:getId()
            count = INT or 1
            pos = POS or itemPos
            delay = MSec        
        }}
        
        teleport or teleportF = POS     teleports to position
                                        "itemPos" = item:getPosition()
                                        "homePos" = player:homePos()
        
        funcSTR = STR           executes a function where parameters are (creature, item, itemEx, fromPos, toPos, fromObject, toObject)
                                if function returns true the actionSystem RETURNS false
                                MODIFICATIONS for 3rd parameter:
                                STR(fromPos) = 3rd parameter is position where player came from
                                
        returnFalse = BOOL      if true then returns false
    }
]]

IDItems = {
    [2344] = {funcSTR = "doesYourCharacterSayTEST1Success"},
}

AIDItems = {
    [1000] = {funcSTR = "fakeFunction"},
}

GREEN = MESSAGE_INFO_DESCR