function executeActionSystem(creature, item, t, itemEx, fromPos, toPos, fromObject, toObject)
    local functionStr = t.funcSTR or t.funcStr
    _G[functionStr](creature, item, itemEx, fromPos, toPos, fromObject, toObject)
end

function doesYourCharacterSayTEST1Success(player)
    player:sendTextMessage(GREEN, "Test 1 success")
    player:sendTextMessage(GREEN, "Test 2: look lamp")
	status_test1 = true
end

function doesYourCharacterSayTEST3Success(player)
    player:sendTextMessage(GREEN, "Test 3 success")
    player:sendTextMessage(GREEN, "All tests completed, how much it costs me :D xD")
end